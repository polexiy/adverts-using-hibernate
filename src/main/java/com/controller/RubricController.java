package com.controller;

import com.domain.Advertisement;
import com.domain.Rubric;
import com.dto.RubricDTO;
import com.service.RubricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController                    // To can convert to JSON
@RequestMapping("rubric")
public class RubricController {

    @Autowired
    private RubricService service;

    @GetMapping("/get/{rubric_id}")
    public Rubric get(@PathVariable("rubric_id") int rubricId) {

        return service.getById(rubricId);
    }

    @PostMapping("/save")
    public void get(@RequestBody Rubric rubric) {

        service.save(rubric);
    }

    @PutMapping("/update")
    public void update(@RequestBody Rubric rubric) {

        service.update(rubric);
    }

    @DeleteMapping("/remove/{rubric_id}")
    public void remove(@PathVariable("rubric_id") int rubricId) {

        service.remove(rubricId);
    }

    @GetMapping("/get_all_rubrics")
    public List<RubricDTO> getAllRubrics() {

        return service.seeAllRubrics();
    }

}