package com.controller;

import com.domain.Advertisement;
import com.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("advertisement")
public class AdvertisementController {

    @Autowired
    private AdvertisementService service;

    @PostMapping("/save")
    public void save(@RequestBody Advertisement advertisement){

        service.save(advertisement);
    }

    @PutMapping("/update")
    public void update(@RequestBody Advertisement advertisement){

        service.update(advertisement);
    }

}
