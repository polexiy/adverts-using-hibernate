package com.controller;

import com.domain.Advertisement;
import com.domain.Author;
import com.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("author")
public class AuthorController {

    @Autowired
    private AuthorService service;

    @GetMapping("/get/{author_id}")
    public Author get(@PathVariable("author_id") int id) {

        return service.getById(id);
    }

    @PostMapping("/save")
    public void save(@RequestBody Author author) {

        service.add(author);
    }

    @DeleteMapping("/delete/{author_id}")
    public void delete(@PathVariable("author_id") int authorId){

        service.remove(authorId);
    }

    @PutMapping("/update")
    public void update(@RequestBody Author author){

        service.update(author);
    }
}
