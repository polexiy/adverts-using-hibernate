package com.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Email {

    @Id
    @GeneratedValue
    private int id;

    @javax.validation.constraints.Email
    @NotNull
    private String email;

    public Email() {
    }

    public Email(@javax.validation.constraints.Email String email) {
        this.email = email;
    }

    @javax.validation.constraints.Email
    @NotNull
    public String getEmail() {

        return email;
    }

    public void setEmail(@NotNull @javax.validation.constraints.Email String email) {

        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
