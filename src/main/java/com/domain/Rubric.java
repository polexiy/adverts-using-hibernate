package com.domain;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Rubric {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "rubric_name")
    @Pattern(regexp = "\\w{2,15}")
    @NotNull
    private String rubricName;

    @OneToMany(mappedBy = "rubric", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
            orphanRemoval = true)
    Set<Advertisement> advertisements = new HashSet<>();

    public Rubric(String rubricName) {
        this.rubricName = rubricName;
    }

    public Rubric() {
    }

    public void addAdvertisement(Advertisement advertisement) {

        advertisements.add(advertisement);
    }

    public void setRubricName(String rubricName) {
        this.rubricName = rubricName;
    }

    public String getRubricName() {
        return rubricName;
    }

    public Set<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(Set<Advertisement> advertisements) {

        this.advertisements = advertisements;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }
}
