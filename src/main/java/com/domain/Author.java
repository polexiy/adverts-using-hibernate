package com.domain;



import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Author {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "name_of_author")
    @Pattern(regexp = "^[A-Z][a-z]{1,10}")
    @NotNull
    private String nameOfAuthor; // remove of

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
    private Address address;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true, fetch = FetchType.EAGER)  // I think I have to use EAGER!!!!
    @JoinTable(name = "author_phone",
            joinColumns = @JoinColumn(name = "author_fk_id"),
            inverseJoinColumns = @JoinColumn(name = "phone_fk_id"))
    @NotNull
    private List<Phone> phones = new ArrayList<>();

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @NotNull
    private Email email;

    @OneToMany(mappedBy = "author", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
            orphanRemoval = true)
    private List<Advertisement> advertisements = new ArrayList<>();

    public Author() {
    }

    public void addAdvertisement(Advertisement advertisement) {

        advertisements.add(advertisement);
    }

    public Author(String nameOfAuthor, Address address, Email email) {

        this.nameOfAuthor = nameOfAuthor;
        this.address = address;
        this.email = email;
    }

    public void addPhone(Phone phone) {

        phones.add(phone);
    }

    public String getNameOfAuthor() {
        return nameOfAuthor;
    }

    public void setNameOfAuthor(String nameOfAuthor) {
        this.nameOfAuthor = nameOfAuthor;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public List<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(List<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public Author(@Pattern(regexp = "^[A-Z][a-z]{1,10}") @NotNull String nameOfAuthor) {

        this.nameOfAuthor = nameOfAuthor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }
}
