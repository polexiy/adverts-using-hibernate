package com.dao;




import com.domain.Rubric;
import com.dto.RubricDTO;

import java.util.List;

public interface RubricDAO {

    void save(Rubric rubric);

    Rubric getById(int id);

    void update(Rubric rubric);

    void remove(int id);

    List<RubricDTO> seeAllRubrics();
}
