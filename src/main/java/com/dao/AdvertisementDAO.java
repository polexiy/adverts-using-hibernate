package com.dao;


import com.domain.Advertisement;

import java.time.LocalDate;
import java.util.List;

public interface AdvertisementDAO {

    void save(Advertisement advertisement);

    List<Advertisement> getAll();

    Advertisement getById(int id);

    void update(Advertisement advertisement);

    void remove(int id);

    void showAllByDate(LocalDate date);
}
