package com.dao;


import com.domain.Author;

import java.util.List;

public interface AuthorDAO {

    void add(Author author);

    List<Author> getAll();

    Author getById(int id);

    void update(Author author);

    void remove(int id);

    void deleteAllTheAdverts(int id);
}
