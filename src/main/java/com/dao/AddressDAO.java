package com.dao;


import com.domain.Address;

import java.util.List;

public interface AddressDAO {

    void add(Address address);

    List<Address> getAll();

    Address getById(int id);

    void update(Address address);

    void remove(int id);
}
