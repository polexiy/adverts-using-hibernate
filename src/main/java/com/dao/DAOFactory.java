package com.dao;



public abstract class DAOFactory {

    public abstract AdvertisementDAO getAdvertisementDAO();

    public abstract AuthorDAO getAuthorDAO();

    public abstract RubricDAO getRubricDAO();

    public static DAOFactory getDaoFactory(DataServer dataServer) {
        switch (dataServer) {
            case MYSQL:
                return new MySqlFactory();

        }
        return null;
    }
}

