package com.dao;


import com.service.AdvertisementService;
import com.service.AuthorService;
import com.service.RubricService;

public class MySqlFactory extends DAOFactory {

    @Override
    public AdvertisementDAO getAdvertisementDAO() {

        return new AdvertisementService();
    }

    @Override
    public AuthorDAO getAuthorDAO() {

        return new AuthorService();
    }

    @Override
    public RubricDAO getRubricDAO() {

        return new RubricService();
    }
}
