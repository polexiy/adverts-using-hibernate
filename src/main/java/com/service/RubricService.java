package com.service;


import com.dao.RubricDAO;
import com.domain.Rubric;
import com.dto.RubricDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RubricService implements RubricDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void save(Rubric rubric) {

        em.persist(rubric);
    }


    @Override
    public Rubric getById(int id) {

        Query query = em.createQuery("FROM Rubric r JOIN FETCH r.advertisements WHERE r.id = :id");

        query.setParameter("id", id);

        return (Rubric) query.getSingleResult();

    }

    @Override
    public void update(Rubric rubric) {

        Rubric merge = em.merge(rubric);

        em.persist(merge);
    }

    @Override
    public void remove(int id) {

        Rubric rubric = em.find(Rubric.class, id);

        em.remove(rubric);
    }

    @Override
    public List<RubricDTO> seeAllRubrics() {

        List<RubricDTO> rubricDTOS = new ArrayList<>();

        List<Rubric> rubrics = em.createQuery("FROM Rubric r").getResultList();

        rubrics.stream().forEach(rubric -> rubricDTOS.add(new RubricDTO(rubric.getId(), rubric.getRubricName())));

        return rubricDTOS;
    }
}
