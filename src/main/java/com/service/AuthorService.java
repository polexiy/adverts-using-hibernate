package com.service;


import com.dao.AuthorDAO;
import com.domain.Author;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Service
@Transactional
public class AuthorService implements AuthorDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void add(Author author) {

        em.persist(author);
    }

    @Override
    public List<Author> getAll() {

        Query query = em.createQuery("FROM Author ");

        List<Author> authors = query.getResultList();

        return authors;
    }

    @Override
    public Author getById(int id) {

        Query query = em.createQuery("FROM Author a JOIN FETCH a.advertisements WHERE a.id = " + id);

        Author author = (Author) query.getSingleResult();

        return author;
    }

    @Override
    public void update(Author author) {
        Author merge = em.merge(author);

        em.persist(merge);
    }

    @Override
    public void remove(int id) {

        Author author = em.find(Author.class, id);

        em.remove(author);
    }

    @Override
    public void deleteAllTheAdverts(int id) {

        Author author = em.find(Author.class, id);

        author.getAdvertisements().clear();
    }
}
