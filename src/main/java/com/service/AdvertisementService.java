package com.service;


import com.dao.AdvertisementDAO;
import com.domain.Advertisement;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class AdvertisementService implements AdvertisementDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void save(Advertisement advertisement) {

        em.persist(advertisement);

    }

    @Override
    public List<Advertisement> getAll() {

        Query query = em.createQuery("FROM Advertisement ");

        List<Advertisement> advertisements = query.getResultList();

        return advertisements;
    }

    @Override
    public Advertisement getById(int id) {

        Advertisement advertisement = em.find(Advertisement.class, id);

        return advertisement;

    }

    @Override
    public void update(Advertisement advertisement) {

        /*Query query = em.createQuery("UPDATE Advertisement SET" +
                " rubric_id_fk = " + advertisement.getRubric().getId() +
                ", advert_author_id_fk = " + advertisement.getAuthor().getId() +
                ", date_publish = '" + advertisement.getDatePublish() + "'" +
                ", description_ad = '" + advertisement.getDescriptionAd() + "'" +
                ", price = " + advertisement.getPrice() +
                ", name_ad = '" + advertisement.getName() + "'" +
                " WHERE id = " + advertisement.getId());

        query.executeUpdate();*/

        Advertisement merge = em.merge(advertisement);

        em.persist(merge);
    }

    @Override
    public void remove(int id) {

        Advertisement advertisement = em.find(Advertisement.class, id);

        em.remove(advertisement);
    }

    @Override
    public void showAllByDate(LocalDate date) {

        Query query = em.createQuery("FROM Advertisement WHERE datePublish = :gotDate");

        query.setParameter("gotDate", date);

        query.getResultList().stream().forEach(System.out::println);

    }
}
