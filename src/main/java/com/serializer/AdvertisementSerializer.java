package com.serializer;

import com.domain.Advertisement;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class AdvertisementSerializer extends StdSerializer<Advertisement> {
    protected AdvertisementSerializer(Class<Advertisement> t) {
        super(t);
    }

    public AdvertisementSerializer() {
        this(null);
    }

    @Override
    public void serialize(Advertisement advertisement, JsonGenerator gen, SerializerProvider provider) throws IOException {

        gen.writeStartObject();
        gen.writeObjectField("id", advertisement.getId());
        gen.writeObjectField("price", advertisement.getPrice());
        gen.writeObjectField("author_id", advertisement.getAuthor().getId());
        gen.writeObjectField("rubric_id", advertisement.getRubric().getId());
        gen.writeStringField("name", advertisement.getName());
        gen.writeStringField("descriptionAd", advertisement.getDescriptionAd());

        gen.writeEndObject();
    }
}
