package com.dto;

public class RubricDTO {

    private int id;

    private String rubricName;

    public RubricDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRubricName() {
        return rubricName;
    }

    public void setRubricName(String rubricName) {
        this.rubricName = rubricName;
    }

    public RubricDTO(int id, String rubricName) {
        this.id = id;
        this.rubricName = rubricName;
    }
}
